{
  request
  createServer
} = require "http"

{
  createHash
} = require "crypto"

{
  floor
  pow
  random
} = Math

md5 = (data)->
  createHash "md5"
  .update data
  .digest "hex"

parseDigest = (digs)->
  m = digs.match /^Digest\s+(.+)$/m
  return unless m?[1]?
  opts = {}
  for m in m[1].split /\s*,\s*/m
    m = m.split /\s*=\s*/
    if m?.length is 2
      [k, v] = m
      v = v
      .replace /^["']/, ""
      .replace /["']$/, ""
      opts[k] = v
  opts

renderDigest = (opts = {})->
  opts.realm ?= "realm"
  opts.qop ?= "auth"
  "Digest " + ("#{key}=\"#{val}\"" for key, val of opts).join ", "

parseNC = (nc)->
  b = new Buffer nc, "hex"
  b.readUInt32BE 0

renderNC = (nc)->
  b = new Buffer 4
  b.writeUInt32BE nc, 0
  b.toString "hex"

maxNC = (pow 2, 32) - 1

randomNC = ->
  renderNC floor maxNC * do random

createDigestResponse = (opts = {})->
  opts.opaque ?= md5 do randomNC
  opts.nonce = md5 do randomNC
  
  renderDigest opts

createDigestRequest = (opts = {})->
  opts.username ?= "test"
  opts.password ?= "test"
  opts.method ?= "GET"
  opts.uri ?= "/"
  opts.qop ?= "auth"
  
  throw new Error "nonce required" unless opts.nonce?
  throw new Error "opaque required" unless opts.opaque?
  
  opts.nc = renderNC if opts.nc? then 1 + parseNC opts.nc else 1
  opts.cnonce = do randomNC
  
  ha1 = md5 "#{opts.username}:#{opts.realm}:#{opts.password}"
  ha2 = md5 "#{opts.method}:#{opts.uri}"
  
  opts.response = md5 "#{ha1}:#{opts.nonce}:#{opts.nc}:#{opts.cnonce}:#{opts.qop}:#{ha2}"
  
  delete opts.method
  delete opts.password
  
  renderDigest opts

users =
  john: "Ahp4ohpa"
  maria: "aijie8Oo"
  test: "test"

verifyDigestRequest = (digs)->
  opts = parseDigest digs
  
  opts.method ?= "GET"
  opts.uri ?= "/"
  
  password = users[opts.username]
  return no unless password?
  
  ha1 = md5 "#{opts.username}:#{opts.realm}:#{password}"
  ha2 = md5 "#{opts.method}:#{opts.uri}"
  
  response = md5 "#{ha1}:#{opts.nonce}:#{opts.nc}:#{opts.cnonce}:#{opts.qop}:#{ha2}"
  
  response is opts.response

local_test = ->
  console.log "response", res = do createDigestResponse

  opts = parseDigest res

  opts.username = "john"
  opts.password = "Ahp4ohpa"

  console.log "request 1:", req = createDigestRequest opts

  console.log "verify request 1:", yes is verifyDigestRequest req

  opts.username = "maria"

  console.log "request 2:", req = createDigestRequest opts

  console.log "verify request 2:", no is verifyDigestRequest req

server = ->
  createServer (req, res)->
    auth = req.headers.authorization
    unless auth and verifyDigestRequest auth
      res.setHeader "WWW-Authenticate", do createDigestResponse
      res.writeHead 401, "Unauthorized"
      do res.end
      return
    res.setHeader "Content-Type", "text/html"
    res.writeHead 200, "OK"
    res.end """
      <html>
        <head>
          <script>
            function deauth() {
              try {
                x = new XMLHttpRequest()
                x.open("GET", location.pathname, "_", "_")
                x.send()
              } catch(e) {}
            }
          </script>
        </head>
        <body>
          Success!
          <button onclick=\"javascript:deauth()\">de-auth</button>
        </body>
      </html>
      """
  .listen 8008
  console.log "Server started at port: 8008"

do local_test
do server
